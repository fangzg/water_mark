<?php


namespace water_mark\facade;


use think\Facade;

class WaterMarkService extends Facade
{
    protected static function getFacadeClass()
    {
        return \water_mark\WaterMarkService::class;
    }
}