<?php
declare (strict_types=1);

namespace water_mark;


use think\Exception;

class WaterMarkService
{
    /**
     * 0 = 非图片类型
     * 1 = GIF
     * 2 = JPG
     * 3 = PNG
     * 4 = SWF
     * 5 = PSD
     * 6 = BMP
     * 7 = TIFF(intel byte order)
     * 8 = TIFF(motorola byte order)
     * 9 = JPC
     * 10 = JP2
     * 11 = JPX
     * 12 = JB2
     * 13 = SWC
     * 14 = IFF
     * 15 = WBMP
     * 16 = XBM
     * @var
     */
    protected $typeIndex;

    protected $wmIndex = [2, 3];//由于gif需要另外的扩展，这里没有实现，所以仅支持

    protected $pct = 80;//水印透明度

    //水印图像的外边距
    protected $marge_right = 10;
    protected $marge_bottom = 10;

    /**
     * 水印图片路径
     * @var string
     */
    protected $stampPath;

    /**
     * 水印缩放比例
     * @var int
     */
    protected $range = 10;

    public function __construct()
    {
//        $this->stampPath = public_path('static') . 'wm.png';
        $this->stampPath = __DIR__ . '/libs/wm.png';
    }

    /**
     * @throws Exception
     */
    public function handle($path)
    {
        try {
            $this->getTypeIndex($path);
            if (!in_array($this->typeIndex,$this->wmIndex)) return successResponse();
            $im = $this->getIm($path);
            [$ix,$iy] = $this->getSize($im);
            $stamp = $this->getStamp($ix,$iy);
            [$sx,$sy] = $this->getSize($stamp);
//            imagecopymerge($im, $stamp, $ix - $sx - $this->marge_right, $iy - $sy - $this->marge_bottom, 0, 0, $sx, $sy, $this->pct);
            imagecopy($im, $stamp, $ix - $sx - $this->marge_right, $iy - $sy - $this->marge_bottom, 0, 0, $sx, $sy);
            imagepng($im, $path);
            imagedestroy($stamp);
            imagedestroy($im);
            return successResponse();
        } catch (\Exception $e) {
            return errorResponse($e->getCode(),$e->getMessage());
        }

    }

    /**
     * 等比缩放水印
     * @param $ix
     * @param $iy
     * @return false|\GdImage|resource
     */
    protected function getStamp($ix,$iy)
    {
        // 加载水印
        $o_stamp = imagecreatefrompng($this->stampPath);
        $o_sx = imagesx($o_stamp);
        $o_sy = imagesy($o_stamp);
        //等比缩放水印
        $sy = (int)($iy / $this->range);
        $sx = (int)($o_sx / $o_sy * $sy);
        $stamp = imagecreatetruecolor($sx, $sy);
//        $background = imagecolorallocatealpha($stamp, 0, 0, 0, 127);
//        imagefill($stamp, 0, 0, $background);
//        imagealphablending($stamp,false);//不合并颜色,直接用$stamp图像颜色替换,包括透明色
//        imagesavealpha($stamp, true);

        //设置透明
        $color=imagecolorallocate($stamp,0,0,0);
        imagecolortransparent($stamp,$color);
        imagefill($stamp,0,0,$color);


        imagecopyresized($stamp, $o_stamp, 0, 0, 0, 0, $sx, $sy, $o_sx, $o_sy);

        return $stamp;
    }

    /**
     * @param $im
     * @return array
     */
    protected function getSize($im):array
    {
        return [imagesx($im),imagesy($im)];
    }

    /**
     * @param $path
     */
    protected function getIm($path)
    {
        switch ($this->typeIndex){
            case 2:
                $im = imagecreatefromjpeg($path);
                break;
            case 3:
                $im = imagecreatefrompng($path);
                break;
            default:
                $im = imagecreatefromstring(file_get_contents($path));
        }
        return $im;
    }

    /**
     * @param $path
     * @throws Exception
     */
    protected function getTypeIndex($path): void
    {
        if (file_exists($path)) {
//            [$x, $y, $type_index, $htmlSize, $bits, $channels, $mime] = getimagesize($path);
            $info = getimagesize($path);
            if ($info){
                $this->typeIndex = $info[2];
            }else{
                $this->typeIndex = 0;
//                throw new Exception('fail to get file info ', 400);
            }
        } else {
            throw new Exception('file not exist', 404);
        }
    }
}